// index.ts
var app = getApp()
var utils = require('../../utils/util')

Page({
  data: {
    list: [],
    checked: false,
    nbFrontColor: '#000000',
    nbBackgroundColor: '#ffffff',
    hiddenmodalput: true,
    startX : 0,
    startY : 0,
    showIndex: 20,

    title: '',
    description: '',
    due_time: '时间',
    update: false,
    id: 0,
  },
  onShow: function () {
    utils.getAll().then((res)=>{
      this.data.list = []
      for (let i = 0, j = 0; i < res.length; i = j) {
        let tmpmap = {"time": res[i].due_time, "childlist": []}
        for (j = i; j < res.length && res[j].due_time == res[i].due_time; j++) {
          tmpmap.childlist.push(res[j])
        }
        this.data.list.push(tmpmap)
      }
      this.setData({
        list: this.data.list
      })
      console.log("hello?",this.data.list)
    })
  },
  // ------------------------------
  // 滑动删除部分
  // ------------------------------ 
  // 手指触摸动作开始
  touchStart: function(e){
    //开始触摸时 重置
    this.data.list.forEach(function (v, ) {
      v.childlist.forEach(function (v, ) {
        if (v.isTouchMove) v.isTouchMove = false; // 只操作为true的
      })
    })
    // 记录手指触摸开始坐标
    this.setData({
      startX: e.changedTouches[0].clientX,  // 开始X坐标
      startY: e.changedTouches[0].clientY,  // 开始Y坐标
      list: this.data.list
    })
  },
  
  // 手指触摸后移动
  touchMove: function(e){
    // 定义变量
    let that = this,
        index = e.currentTarget.dataset.index,    // 当前下标
        parentIndex = e.currentTarget.dataset.parentindex,
        startX = this.data.startX,                // 开始X坐标
        startY = this.data.startY,                // 开始Y坐标
        touchMoveX = e.changedTouches[0].clientX, // 滑动变化坐标
        touchMoveY = e.changedTouches[0].clientY, // 滑动变化坐标
        angle = that.angle({ X: startX, Y: startY }, { X: touchMoveX, Y: touchMoveY }); // 获取滑动角度

    // 判断滑动角度
    this.data.list.forEach(function (v, j) {
      v.childlist.forEach(function (v, i) {
        v.isTouchMove = false
        // 滑动超过30度角 return
        if (Math.abs(angle) > 30) return;
        if (i == index && j == parentIndex) {
          if (touchMoveX > startX) // 右滑
            v.isTouchMove = false // 左滑
          else 
            v.isTouchMove = true
        }
      })
    })

    // 更新数据
    that.setData({
      list: this.data.list
    })
  },
  
  // 计算滑动角度
  angle: function (start, end) {
    let _X = end.X - start.X,
        _Y = end.Y - start.Y;
    // 返回角度 /Math.atan()返回数字的反正切值
    return 360 * Math.atan(_Y / _X) / (2 * Math.PI);
  },
  change_is_completed: function (e) {
    let index = e.currentTarget.dataset.index;  
    let lindex = e.currentTarget.dataset.parentindex;
    this.data.list[lindex].childlist[index].is_completed = !this.data.list[lindex].childlist[index].is_completed
    this.setData({
      list: this.data.list
    })
    utils.postCheck({'id': this.data.list[lindex].childlist[index].id})
  },
  // 删除
  delList: function(e){
    let index = e.currentTarget.dataset.index;  // 当前下标
    let lindex = e.currentTarget.dataset.parentindex;
　　　　　// 切割当前下标元素，更新数据
    utils.postDelete({'id': this.data.list[lindex].childlist[index].id})
    console.log("test",index, this.data.list[lindex].childlist);
    this.data.list[lindex].childlist.splice(index, 1); 
    if (!this.data.list[lindex].childlist.length) {
      this.data.list.splice(lindex, 1);
    }
    this.setData({
      list: this.data.list,
      checked: false
    })
},
  
  // ------------------------------
  // 弹窗部分
  // ------------------------------ 
  update: function (e) {
    let index = e.currentTarget.dataset.index;  
    let lindex = e.currentTarget.dataset.parentindex;
    this.setData({
      showModal: true,
      index: index,
      lindex: lindex,
      title: this.data.list[lindex].childlist[index].title,
      description: this.data.list[lindex].childlist[index].description,
      due_time: this.data.list[lindex].childlist[index].due_time,
      id : this.data.list[lindex].childlist[index].id,
      update: true
    })
    console.log("first", lindex, index, this.data.id)
  },
  showDialogBtn: function () {
    this.setData({
      showModal: true,
      //list: app.globalData.list
    })
  },
  
  // 弹出框蒙层截断touchmove事件
  preventTouchMove: function () {
  },
  
  // 隐藏模态对话框
  hideModal: function () {
    this.setData({
      showModal: false,
      title: '',
      description: '',
      due_time: '时间',
      list: this.data.list,
      update: false
    });
  },

  // 对话框取消按钮点击事件
  onCancel: function () {
    this.hideModal();
  },

  // 获取input输入
  titleinput: function(e) {
    this.setData({ title : e.detail.value })
  },
  despinput: function(e) {
    this.setData({ description : e.detail.value })
  },
  bindDateChange: function (e) {
    this.setData({
      due_time: e.detail.value
    })
  },
  // 对话框确认按钮点击事件
  onConfirm: function () {
    if (!this.data.update) {
        //新增
      utils.postAdd({'title': this.data.title, 'description': this.data.description, 'due_time': this.data.due_time}).then((res)=>{
        let flag = false;
        let index = 0;
        console.log(res);
        for(; index < this.data.list.length; index++) {
          if (this.data.list[index].time == this.data.due_time) {
            this.data.list[index].childlist.push(res);
            flag = true;
            break;
          } else
          if (this.data.list[index].time > this.data.due_time) break;
        }
        if (!flag) {
          let tmpmap = {"time": res.due_time, "childlist": []}
          tmpmap.childlist.push(res);
          this.data.list.splice(index, 0, tmpmap);
        }
        this.hideModal();
      })
    } else {
      //修改
      utils.postUpdate({
        'id': this.data.id, 'title': this.data.title, 'description': this.data.description, 'due_time': this.data.due_time
      })
      let lindex = this.data.lindex, index = this.data.index;
      console.log("second", lindex, index, this.data.id)
      let tmp = this.data.list[lindex].childlist[index];
      tmp.title = this.data.title;
      tmp.description = this.data.description;
      if (tmp.due_time == this.data.due_time) {
        this.data.list[lindex].childlist.splice(index, 1, tmp);
      } else {
        if (this.data.list[lindex].childlist.length == 1)
          this.data.list.splice(lindex, 1);
        else
          this.data.list[lindex].childlist.splice(index, 1);
        tmp.due_time = this.data.due_time;
        let i = 0, flag = false;
        for (; i < this.data.list.length; i++) {
          if (this.data.list[i].time == tmp.due_time) {
            flag = true;
            this.data.list[i].childlist.push(tmp);
            break;
          } else
          if (this.data.list[i].time > tmp.due_time) break;
        }
        if (!flag) {
          let tmpmap = {"time": tmp.due_time, "childlist": []}
          tmpmap.childlist.push(tmp);
          this.data.list.splice(i, 0, tmpmap);
        }
      }
      this.hideModal();
    }
  },

  // ------------------------------
  // 折叠展开
  // ------------------------------ 
  panel: function (e) {
    this.data.list[e.currentTarget.dataset.index].isopen = !this.data.list[e.currentTarget.dataset.index].isopen
    this.setData({
      list:this.data.list
    })
    if (e.currentTarget.dataset.index != this.data.showIndex) {
      this.setData({
        showIndex: e.currentTarget.dataset.index,
      })
    } 
    else {
      this.setData({
        showIndex: 20
      })
    }
  },
})
  