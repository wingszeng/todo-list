// index.ts
/*const date = new Date()
const years = []
const months = []
const days = []*/
var app = getApp()
var utils = require('../../utils/util.js')
/*for (let i = 1990; i <= date.getFullYear(); i++) {
  years.push(i)
}
for (let i = 1; i <= 12; i++) {
  months.push(i)
}
for (let i = 1; i <= 31; i++) {
  days.push(i)
}*/
Page({
  data: {
    list: {
      time: '',
      childlist : [
        //{'id': 1, 'title': 'AAA', 'description': 'aaa', 'due_time': ''},
        //{'id': 2, 'title': 'BBB', 'description': 'bbb'},
        //{'id': 3, 'title': 'CCC', 'description': 'ccc'},
      ] 
    },
    checked: false,
    nbFrontColor: '#000000',
    nbBackgroundColor: '#ffffff',
    hiddenmodalput: true,
    startX : 0,
    startY : 0,

    // 获得新建事件弹窗输入
    title: '',
    description: '',
    due_time: '时间',
    update: false,
    id: 0,
  },
  
/*  watchBack: function (name : any,value : any) {
      this.setData({
          list: getApp().globalData.list[0],
      });
  },
*/
  onShow: function () {
    utils.getAll().then((res)=>{
      let TIME = utils.formatDate(new Date())
      this.setData({
        'list.time' : TIME
      })
      let tmplist = []
      for(let index = 0; index < res.length; index++){
        //res[index].due_time = utils.formatDate(new Date(res[index].due_time))
        if(res[index].due_time == TIME){
          tmplist.push(res[index]);
        }
      }
      console.log("tmplist",tmplist);
      this.setData({
        "list.childlist": tmplist
      })
    })
  },

  jumpToAllList: function() {
    wx.navigateTo({url: '../overview/overview'})
  },

  // 滑动删除部分
    // 手指触摸动作开始
    touchStart: function(e){
      //开始触摸时 重置
      this.data.list.childlist.forEach(function (v) {
          if (v.isTouchMove) v.isTouchMove = false; // 只操作为true的
      })
      // 记录手指触摸开始坐标
      this.setData({
          startX: e.changedTouches[0].clientX,  // 开始X坐标
          startY: e.changedTouches[0].clientY,  // 开始Y坐标
          list: this.data.list
      })
      //console.log(this.data.startX);
  },

  // 手指触摸后移动
  touchMove: function(e){
      let that = this,
          index = e.currentTarget.dataset.index,    // 当前下标
          startX = this.data.startX,                // 开始X坐标
          startY = this.data.startY,                // 开始Y坐标
          touchMoveX = e.changedTouches[0].clientX, // 滑动变化坐标
          touchMoveY = e.changedTouches[0].clientY, // 滑动变化坐标
          // 获取滑动角度
          angle = that.angle({ X: startX, Y: startY }, { X: touchMoveX, Y: touchMoveY });
　　　　　// 判断滑动角度
      this.data.list.childlist.forEach(function (v, i) {
          v.isTouchMove = false
          // 滑动超过30度角 return
          if (Math.abs(angle) > 30) return;
          if (i == index) {
              // 右滑
              if (touchMoveX > startX) 
                  v.isTouchMove = false
              // 左滑
              else 
                  v.isTouchMove = true
          }
    })
    // 更新数据
    that.setData({
        list: this.data.list
    })
  },

  // 计算滑动角度
  angle: function (start , end) {
        let  _X = end.X - start.X,
          _Y = end.Y - start.Y;
      // 返回角度 /Math.atan()返回数字的反正切值
      return 360 * Math.atan(_Y / _X) / (2 * Math.PI);
  },

  // 删除
  change_is_completed: function(e) {
    let index = e.currentTarget.dataset.index
    this.data.list.childlist[index].is_completed = !this.data.list.childlist[index].is_completed
    this.setData({
      list: this.data.list
    })
    utils.postCheck({"id": this.data.list.childlist[index].id});
  },
  delList: function(e){
      let index = e.currentTarget.dataset.index;  // 当前下标
　　　　　// 切割当前下标元素，更新数据
      console.log("test",index, this.data.list.childlist);
      utils.postDelete({'id': this.data.list.childlist[index].id})
      this.data.list.childlist.splice(index, 1); 
      this.setData({
          list: this.data.list
      })
      this.setData({
        "checked": false
      })
  },

  // --------------------
  // 弹窗部分
  // --------------------
  update: function (e) {
    let index = e.currentTarget.dataset.index;
    this.setData({
      showModal: true,
      index: index,
      title: this.data.list.childlist[index].title,
      description: this.data.list.childlist[index].description,
      due_time: this.data.list.childlist[index].due_time,
      id: this.data.list.childlist[index].id,
      update: true
    })
   },
  showDialogBtn: function () {
    this.setData({
      showModal: true,
      //list: app.globalData.list[0]
    })
  },

  /*bindChange(e) {
    const val = e.detail.value
    this.setData({
      year: this.data.years[val[0]],
      month: this.data.months[val[1]],
      day: this.data.days[val[2]],
      isDaytime: !val[3]
    })
  },*/

  // 弹出框蒙层截断touchmove事件
  preventTouchMove: function () {
  },

  // 隐藏模态对话框
  hideModal: function () {
    this.setData({
      showModal: false,
      title: '',
      description: '',
      due_time: '时间',
      list: this.data.list,
      update: false
    });
  },

  // 对话框取消按钮点击事件
  onCancel: function () {
    this.hideModal();
  },

  // 获取input输入
  titleinput: function(e) {
    this.setData({ title : e.detail.value })
  },
  despinput: function(e) {
    this.setData({ description : e.detail.value })
  },
  bindDateChange: function (e) {
    this.setData({
      due_time: e.detail.value
    })
  },
  // 对话框确认按钮点击事件
  onConfirm: function () {
    //console.log(this.data.list)
    if (!this.data.update) {
      utils.postAdd({'title': this.data.title, 'description': this.data.description, 'due_time': this.data.due_time}).then((res)=>{
        if (res.due_time == this.data.list.time) {
          this.data.list.childlist.push(res)
        }
        this.hideModal();
      })
    } else {
      utils.postUpdate({
        'id': this.data.id, 'title': this.data.title, 'description': this.data.description, 'due_time': this.data.due_time
      })
      let index  = this.data.index, tmp = this.data.list.childlist[index];
      console.log(index, tmp);
      tmp.description = this.data.description;
      tmp.title = this.data.title;
      if (tmp.due_time == this.data.due_time) {
        this.data.list.childlist.splice(index, 1, tmp);
      } else {
        this.data.list.childlist.splice(index, 1);
      }
      this.hideModal();
    }
  },
})
